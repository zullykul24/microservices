package com.edso.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {

    @GetMapping("/cryptoServiceFallBack")
    public String cryptoServiceFallBackMethod(){
        return "Crypto service is taking longer than expected." +
                " Please try again later";
    }
}
