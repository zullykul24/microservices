/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : cryto

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 25/10/2021 02:15:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for crypto
-- ----------------------------
DROP TABLE IF EXISTS `crypto`;
CREATE TABLE `crypto`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL,
  `is_deleted` bit(1) NULL DEFAULT NULL,
  `whitelist_collateral` bit(1) NULL DEFAULT NULL,
  `whitelist_supply` bit(1) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `coin_gecko_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `coin_gecko_id`(`coin_gecko_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of crypto
-- ----------------------------
INSERT INTO `crypto` VALUES (1, 'DFY', '0x20f1dE452e9057fe863b99d33CF82DBeE0C45B14', NULL, NULL, b'0', b'1', b'1', 'Defi For You', 'defi-for-you-disabled');
INSERT INTO `crypto` VALUES (2, 'BTC', '0x52D2848E79861492795a2635B76493999F1EdB1F', NULL, NULL, b'0', b'1', b'0', 'Bitcoin', 'bitcoin');
INSERT INTO `crypto` VALUES (3, 'eth', '0x11eth', NULL, NULL, b'0', b'0', b'0', 'Ethereum', 'ethereum');
INSERT INTO `crypto` VALUES (4, 'WBNB', '0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd', NULL, NULL, b'0', b'1', b'0', NULL, 'binancecoin');
INSERT INTO `crypto` VALUES (5, 'USDT', '0x362F62b601E80221abb8013c496055fbD297b0B5', NULL, NULL, b'0', b'0', b'1', 'Tether', 'tether');
INSERT INTO `crypto` VALUES (6, 'xrp', '0x11xrp', NULL, NULL, b'0', b'0', b'0', 'xrp', 'ripple');
INSERT INTO `crypto` VALUES (7, 'LTC', '0x395998D21d4Dd230f38247F84145Bd4d9afAaCa2', NULL, NULL, b'0', b'1', b'0', 'Litecoin', 'litecoin');
INSERT INTO `crypto` VALUES (8, 'ETC', '0x3d6545b08693daE087E957cb1180ee38B9e3c25E', NULL, NULL, b'0', b'0', b'0', 'Ethereum Classic', 'ethereum-classic');
INSERT INTO `crypto` VALUES (9, 'ZEC', '0x1Ba42e5193dfA8B03D15dd1B86a3113bbBEF8Eeb', NULL, NULL, b'0', b'0', b'0', 'Zcash', 'zcash');
INSERT INTO `crypto` VALUES (10, 'MKR', '0x5f0Da599BB2ccCfcf6Fdfd7D81743B6020864350', NULL, NULL, b'0', b'0', b'0', 'Maker', 'maker');
INSERT INTO `crypto` VALUES (11, 'BAT', '0x101d82428437127bF1608F699CD651e6Abf9766E', NULL, NULL, b'0', b'0', b'0', 'Basic Attention Token', 'basic-attention-token');
INSERT INTO `crypto` VALUES (12, 'EOS', '0x56b6fB708fC5732DEC1Afc8D8556423A2EDcCbD6', NULL, NULL, b'0', b'0', b'0', 'EOS', 'eos');
INSERT INTO `crypto` VALUES (13, 'BCH', '0x8fF795a6F4D97E7887C79beA79aba5cc76444aDf', NULL, NULL, b'0', b'0', b'0', 'Bitcoin Cash', 'bitcoin-cash');
INSERT INTO `crypto` VALUES (14, 'LINK', '0xF8A0BF9cF54Bb92F17374d9e9A321E6a111a51bD', NULL, NULL, b'0', b'0', b'0', 'Chainlink', 'chainlink');
INSERT INTO `crypto` VALUES (15, 'ADA', '0xCE89CFcca3fedD566C7574902137eD31A4C9Ea40', NULL, NULL, b'0', b'1', b'0', 'Cardano', 'cardano');
INSERT INTO `crypto` VALUES (16, 'XTZ', '0x16939ef78684453bfDFb47825F8a5F714f12623a', NULL, NULL, b'0', b'0', b'0', 'Tezos', 'tezos');
INSERT INTO `crypto` VALUES (17, 'FIL', '0x0D8Ce2A99Bb6e3B7Db580eD848240e4a0F9aE153', NULL, NULL, b'0', b'0', b'0', 'Filecoin', 'filecoin');
INSERT INTO `crypto` VALUES (18, 'ELF', '0xa3f020a5C92e15be13CAF0Ee5C95cF79585EeCC9', NULL, NULL, b'0', b'0', b'0', 'aelf', 'aelf');
INSERT INTO `crypto` VALUES (19, 'TCT', '0xCA0a9Df6a8cAD800046C1DDc5755810718b65C44', NULL, NULL, b'0', b'0', b'0', 'TokenClub', 'tokenclub');
INSERT INTO `crypto` VALUES (20, 'ONT', '0xFd7B3A77848f1C2D67E05E54d78d174a0C850335', NULL, NULL, b'0', b'0', b'0', 'Ontology', 'ontology');
INSERT INTO `crypto` VALUES (21, 'SNX', '0x9Ac983826058b8a9C7Aa1C9171441191232E8404', NULL, NULL, b'0', b'0', b'0', 'Synthetix', 'havven');
INSERT INTO `crypto` VALUES (22, 'IOTX', '0x9678E42ceBEb63F23197D726B29b1CB20d0064E5', NULL, NULL, b'0', b'0', b'0', 'IoTeX', 'iotex');
INSERT INTO `crypto` VALUES (23, 'USDP', '0xb7F8Cd00C5A06c0537E2aBfF0b58033d02e5E094', NULL, NULL, b'0', b'0', b'0', 'Paxos Standard', 'paxos-standard');
INSERT INTO `crypto` VALUES (24, 'USDC', '0xF02224B084f76fFd3427c8DD3055ea0dEA7F1EC2', NULL, NULL, b'0', b'0', b'1', 'USD Coin', 'usd-coin');
INSERT INTO `crypto` VALUES (25, 'ATOM', '0x0Eb3a705fc54725037CC9e008bDede697f62F335', NULL, NULL, b'0', b'0', b'0', 'Cosmos', 'cosmos');
INSERT INTO `crypto` VALUES (26, 'SXP', '0x47BEAd2563dCBf3bF2c9407fEa4dC236fAbA485A', NULL, NULL, b'0', b'0', b'0', 'Swipe', 'swipe');
INSERT INTO `crypto` VALUES (27, 'BAND', '0xAD6cAEb32CD2c308980a548bD0Bc5AA4306c6c18', NULL, NULL, b'0', b'0', b'0', 'Band Protocol', 'band-protocol');
INSERT INTO `crypto` VALUES (28, 'BUSD', '0x544D0AD6BdDa001630941f6523D935E32cCAb604', NULL, NULL, b'0', b'0', b'1', 'Binance USD', 'binance-usd');
INSERT INTO `crypto` VALUES (29, 'DAI', '0xaeeFfB36f4F8f0603AF63607f839D9457fc8b292', NULL, NULL, b'0', b'0', b'1', 'Dai', 'dai');
INSERT INTO `crypto` VALUES (30, 'COMP', '0x52CE071Bd9b1C4B00A0b92D298c512478CaD67e8', NULL, NULL, b'0', b'0', b'0', 'Compound', 'compound-governance-token');
INSERT INTO `crypto` VALUES (31, 'YFI', '0x88f1A5ae2A3BF98AEAF342D26B30a79438c9142e', NULL, NULL, b'0', b'0', b'0', 'yearn.finance', 'yearn-finance');
INSERT INTO `crypto` VALUES (32, 'YFII', '0x7F70642d88cf1C4a3a7abb072B53B929b653edA5', NULL, NULL, b'0', b'0', b'0', 'DFI.Money', 'yfii-finance');
INSERT INTO `crypto` VALUES (33, 'NEAR', '0x1Fa4a73a3F0133f0025378af00236f3aBDEE5D63', NULL, NULL, b'0', b'0', b'0', 'NEAR Protocol', 'near');
INSERT INTO `crypto` VALUES (34, 'DOT', '0x62950ba1AdB2C867A24599a3666bf83Cf84c29Ed', NULL, NULL, b'0', b'1', b'0', 'Polkadot', 'polkadot');
INSERT INTO `crypto` VALUES (35, 'BEL', '0x8443f091997f06a61670B735ED92734F5628692F', NULL, NULL, b'0', b'0', b'0', 'Bella Protocol', 'bella-protocol');
INSERT INTO `crypto` VALUES (36, 'UNI', '0xBf5140A22578168FD562DCcF235E5D43A02ce9B1', NULL, NULL, b'0', b'0', b'0', 'Uniswap', 'uniswap');
INSERT INTO `crypto` VALUES (37, 'INJ', '0xa2B726B1145A4773F68593CF171187d8EBe4d495', NULL, NULL, b'0', b'0', b'0', 'Injective Protocol', 'injective-protocol');
INSERT INTO `crypto` VALUES (38, 'BNB', '0x0000000000000000000000000000000000000000', NULL, NULL, b'0', b'1', b'0', 'Binance Coin', 'binancecoin');

-- ----------------------------
-- Table structure for crypto_rate_history
-- ----------------------------
DROP TABLE IF EXISTS `crypto_rate_history`;
CREATE TABLE `crypto_rate_history`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `crypto_id` bigint(0) NOT NULL,
  `current_price` decimal(10, 2) NULL DEFAULT NULL,
  `saved_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_crypto_id`(`crypto_id`) USING BTREE,
  CONSTRAINT `fk_crypto_id` FOREIGN KEY (`crypto_id`) REFERENCES `crypto` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1945 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of crypto_rate_history
-- ----------------------------
INSERT INTO `crypto_rate_history` VALUES (1801, 2, 60770.00, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1802, 3, 4091.33, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1803, 38, 480.16, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1804, 5, 1.01, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1805, 15, 2.14, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1806, 6, 1.08, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1807, 34, 43.50, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1808, 24, 1.00, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1809, 14, 30.35, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1810, 36, 26.60, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1811, 7, 195.34, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1812, 28, 1.00, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1813, 13, 621.64, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1814, 25, 35.19, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1815, 17, 64.34, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1816, 29, 1.01, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1817, 8, 55.01, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1818, 16, 6.68, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1819, 33, 9.70, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1820, 12, 4.90, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1821, 10, 2503.86, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1822, 9, 187.13, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1823, 30, 313.90, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1824, 21, 9.76, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1825, 31, 35455.00, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1826, 11, 0.71, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1827, 23, 1.00, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1828, 20, 0.95, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1829, 22, 0.07, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1830, 37, 12.60, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1831, 26, 2.29, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1832, 27, 9.18, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1833, 18, 0.59, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1834, 32, 4029.76, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1835, 35, 2.34, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1836, 19, 0.04, '2021-10-24 15:51:49');
INSERT INTO `crypto_rate_history` VALUES (1837, 2, 60871.00, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1838, 3, 4096.64, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1839, 38, 480.79, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1840, 5, 1.01, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1841, 15, 2.15, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1842, 6, 1.08, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1843, 34, 43.54, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1844, 24, 1.00, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1845, 14, 30.41, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1846, 36, 26.60, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1847, 7, 195.73, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1848, 28, 1.00, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1849, 13, 623.12, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1850, 25, 35.26, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1851, 17, 64.41, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1852, 29, 1.01, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1853, 8, 55.13, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1854, 16, 6.69, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1855, 33, 9.81, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1856, 12, 4.91, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1857, 10, 2503.76, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1858, 9, 187.25, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1859, 30, 314.65, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1860, 21, 9.76, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1861, 31, 35470.00, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1862, 11, 0.71, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1863, 23, 1.00, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1864, 20, 0.96, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1865, 22, 0.07, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1866, 37, 12.62, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1867, 26, 2.29, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1868, 27, 9.20, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1869, 18, 0.59, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1870, 32, 4039.18, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1871, 35, 2.34, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1872, 19, 0.04, '2021-10-24 16:01:50');
INSERT INTO `crypto_rate_history` VALUES (1873, 2, 61229.00, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1874, 3, 4123.06, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1875, 38, 481.19, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1876, 5, 1.01, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1877, 15, 2.15, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1878, 6, 1.09, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1879, 34, 43.68, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1880, 24, 1.00, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1881, 14, 30.50, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1882, 36, 26.75, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1883, 7, 197.00, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1884, 28, 1.01, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1885, 13, 628.70, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1886, 25, 35.45, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1887, 17, 64.81, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1888, 29, 1.01, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1889, 8, 55.43, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1890, 16, 6.71, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1891, 33, 9.79, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1892, 12, 5.03, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1893, 10, 2523.05, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1894, 9, 188.65, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1895, 30, 315.97, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1896, 21, 9.81, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1897, 31, 35588.00, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1898, 11, 0.71, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1899, 23, 1.00, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1900, 20, 0.96, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1901, 22, 0.07, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1902, 37, 12.73, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1903, 26, 2.30, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1904, 27, 9.23, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1905, 18, 0.59, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1906, 32, 4093.92, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1907, 35, 2.36, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1908, 19, 0.04, '2021-10-24 16:45:42');
INSERT INTO `crypto_rate_history` VALUES (1909, 2, 61243.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1910, 3, 4123.24, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1911, 38, 482.04, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1912, 5, 1.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1913, 15, 2.15, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1914, 6, 1.09, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1915, 34, 43.69, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1916, 24, 1.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1917, 14, 30.50, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1918, 36, 26.75, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1919, 7, 197.03, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1920, 28, 1.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1921, 13, 628.70, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1922, 25, 35.45, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1923, 17, 64.76, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1924, 29, 1.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1925, 8, 55.43, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1926, 16, 6.71, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1927, 33, 9.78, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1928, 12, 5.03, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1929, 10, 2522.47, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1930, 9, 188.66, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1931, 30, 316.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1932, 21, 9.81, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1933, 31, 35587.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1934, 11, 0.71, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1935, 23, 1.00, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1936, 20, 0.96, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1937, 22, 0.07, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1938, 37, 12.73, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1939, 26, 2.30, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1940, 27, 9.22, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1941, 18, 0.59, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1942, 32, 4092.04, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1943, 35, 2.36, '2021-10-24 16:49:54');
INSERT INTO `crypto_rate_history` VALUES (1944, 19, 0.04, '2021-10-24 16:49:54');

SET FOREIGN_KEY_CHECKS = 1;
