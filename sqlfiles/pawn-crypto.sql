/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : pawn-crypto

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 25/10/2021 02:14:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collateral
-- ----------------------------
DROP TABLE IF EXISTS `collateral`;
CREATE TABLE `collateral`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `currency_id` bigint(0) NOT NULL,
  `amount` decimal(10, 5) NOT NULL,
  `duration` int(0) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` enum('OPEN','ACCEPTED','WITHDRAWN') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `loan_currency_id` bigint(0) NOT NULL,
  `offer_received` int(0) NOT NULL,
  `duration_unit` enum('WEEK','MONTH') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wallet_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_currency_id`(`currency_id`) USING BTREE,
  INDEX `fk_loan_currency_id`(`loan_currency_id`) USING BTREE,
  CONSTRAINT `fk_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `crypto_asset` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_loan_currency_id` FOREIGN KEY (`loan_currency_id`) REFERENCES `crypto_asset` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collateral
-- ----------------------------
INSERT INTO `collateral` VALUES (9, 1, 0.03000, 1, 'string', 'WITHDRAWN', 5, 2, 'WEEK', 'string');
INSERT INTO `collateral` VALUES (11, 1, 0.00400, 1, 'abc', 'WITHDRAWN', 5, 0, 'WEEK', 'string');
INSERT INTO `collateral` VALUES (12, 1, 4.00000, 5000, 'abc', 'ACCEPTED', 5, 2, 'WEEK', 'string');
INSERT INTO `collateral` VALUES (13, 1, 0.01000, 3, 'string', 'ACCEPTED', 5, 1, 'WEEK', '0123x');
INSERT INTO `collateral` VALUES (14, 7, 0.11000, 5, 'string', 'WITHDRAWN', 24, 0, 'WEEK', 'x0x1');
INSERT INTO `collateral` VALUES (15, 1, 0.00100, 3, 'string', 'ACCEPTED', 5, 2, 'WEEK', 'string');
INSERT INTO `collateral` VALUES (16, 1, 0.00100, 3, 'string', 'OPEN', 5, 0, 'WEEK', '0x00');
INSERT INTO `collateral` VALUES (17, 1, 0.00100, 3, 'string', 'OPEN', 24, 1, 'MONTH', '0x00');

-- ----------------------------
-- Table structure for crypto_asset
-- ----------------------------
DROP TABLE IF EXISTS `crypto_asset`;
CREATE TABLE `crypto_asset`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL,
  `is_deleted` bit(1) NULL DEFAULT NULL,
  `whitelist_collateral` bit(1) NULL DEFAULT NULL,
  `whitelist_supply` bit(1) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `coin_gecko_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `coin_gecko_id`(`coin_gecko_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of crypto_asset
-- ----------------------------
INSERT INTO `crypto_asset` VALUES (1, 'DFY', '0x20f1dE452e9057fe863b99d33CF82DBeE0C45B14', NULL, NULL, b'0', b'1', b'1', 'Defi For You', 'defi-for-you-disabled', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/DFY.png');
INSERT INTO `crypto_asset` VALUES (2, 'BTC', '0x52D2848E79861492795a2635B76493999F1EdB1F', NULL, NULL, b'0', b'1', b'0', 'Bitcoin', 'bitcoin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BTC.png');
INSERT INTO `crypto_asset` VALUES (3, 'eth', '0x11eth', NULL, NULL, b'0', b'0', b'0', 'Ethereum', 'ethereum', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ETH.png');
INSERT INTO `crypto_asset` VALUES (4, 'WBNB', '0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd', NULL, NULL, b'0', b'1', b'0', NULL, 'binancecoin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/WBNB.png');
INSERT INTO `crypto_asset` VALUES (5, 'USDT', '0x362F62b601E80221abb8013c496055fbD297b0B5', NULL, NULL, b'0', b'0', b'1', 'Tether', 'tether', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/USDT.png');
INSERT INTO `crypto_asset` VALUES (6, 'xrp', '0x11xrp', NULL, NULL, b'0', b'0', b'0', 'xrp', 'ripple', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/XRP.png');
INSERT INTO `crypto_asset` VALUES (7, 'LTC', '0x395998D21d4Dd230f38247F84145Bd4d9afAaCa2', NULL, NULL, b'0', b'1', b'0', 'Litecoin', 'litecoin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/LTC.png');
INSERT INTO `crypto_asset` VALUES (8, 'ETC', '0x3d6545b08693daE087E957cb1180ee38B9e3c25E', NULL, NULL, b'0', b'0', b'0', 'Ethereum Classic', 'ethereum-classic', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ETC.png');
INSERT INTO `crypto_asset` VALUES (9, 'ZEC', '0x1Ba42e5193dfA8B03D15dd1B86a3113bbBEF8Eeb', NULL, NULL, b'0', b'0', b'0', 'Zcash', 'zcash', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ZEC.png');
INSERT INTO `crypto_asset` VALUES (10, 'MKR', '0x5f0Da599BB2ccCfcf6Fdfd7D81743B6020864350', NULL, NULL, b'0', b'0', b'0', 'Maker', 'maker', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/MKR.png');
INSERT INTO `crypto_asset` VALUES (11, 'BAT', '0x101d82428437127bF1608F699CD651e6Abf9766E', NULL, NULL, b'0', b'0', b'0', 'Basic Attention Token', 'basic-attention-token', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BAT.png');
INSERT INTO `crypto_asset` VALUES (12, 'EOS', '0x56b6fB708fC5732DEC1Afc8D8556423A2EDcCbD6', NULL, NULL, b'0', b'0', b'0', 'EOS', 'eos', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/EOS.png');
INSERT INTO `crypto_asset` VALUES (13, 'BCH', '0x8fF795a6F4D97E7887C79beA79aba5cc76444aDf', NULL, NULL, b'0', b'0', b'0', 'Bitcoin Cash', 'bitcoin-cash', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BCH.png');
INSERT INTO `crypto_asset` VALUES (14, 'LINK', '0xF8A0BF9cF54Bb92F17374d9e9A321E6a111a51bD', NULL, NULL, b'0', b'0', b'0', 'Chainlink', 'chainlink', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/LINK.png');
INSERT INTO `crypto_asset` VALUES (15, 'ADA', '0xCE89CFcca3fedD566C7574902137eD31A4C9Ea40', NULL, NULL, b'0', b'1', b'0', 'Cardano', 'cardano', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ADA.png');
INSERT INTO `crypto_asset` VALUES (16, 'XTZ', '0x16939ef78684453bfDFb47825F8a5F714f12623a', NULL, NULL, b'0', b'0', b'0', 'Tezos', 'tezos', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/XTZ.png');
INSERT INTO `crypto_asset` VALUES (17, 'FIL', '0x0D8Ce2A99Bb6e3B7Db580eD848240e4a0F9aE153', NULL, NULL, b'0', b'0', b'0', 'Filecoin', 'filecoin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/FIL.png');
INSERT INTO `crypto_asset` VALUES (18, 'ELF', '0xa3f020a5C92e15be13CAF0Ee5C95cF79585EeCC9', NULL, NULL, b'0', b'0', b'0', 'aelf', 'aelf', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ELF.png');
INSERT INTO `crypto_asset` VALUES (19, 'TCT', '0xCA0a9Df6a8cAD800046C1DDc5755810718b65C44', NULL, NULL, b'0', b'0', b'0', 'TokenClub', 'tokenclub', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/TCT.png');
INSERT INTO `crypto_asset` VALUES (20, 'ONT', '0xFd7B3A77848f1C2D67E05E54d78d174a0C850335', NULL, NULL, b'0', b'0', b'0', 'Ontology', 'ontology', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ONT.png');
INSERT INTO `crypto_asset` VALUES (21, 'SNX', '0x9Ac983826058b8a9C7Aa1C9171441191232E8404', NULL, NULL, b'0', b'0', b'0', 'Synthetix', 'havven', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/SNX.png');
INSERT INTO `crypto_asset` VALUES (22, 'IOTX', '0x9678E42ceBEb63F23197D726B29b1CB20d0064E5', NULL, NULL, b'0', b'0', b'0', 'IoTeX', 'iotex', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/IOTX.png');
INSERT INTO `crypto_asset` VALUES (23, 'USDP', '0xb7F8Cd00C5A06c0537E2aBfF0b58033d02e5E094', NULL, NULL, b'0', b'0', b'0', 'Paxos Standard', 'paxos-standard', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/PAX.png');
INSERT INTO `crypto_asset` VALUES (24, 'USDC', '0xF02224B084f76fFd3427c8DD3055ea0dEA7F1EC2', NULL, NULL, b'0', b'0', b'1', 'USD Coin', 'usd-coin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/USDC.png');
INSERT INTO `crypto_asset` VALUES (25, 'ATOM', '0x0Eb3a705fc54725037CC9e008bDede697f62F335', NULL, NULL, b'0', b'0', b'0', 'Cosmos', 'cosmos', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/ATOM.png');
INSERT INTO `crypto_asset` VALUES (26, 'SXP', '0x47BEAd2563dCBf3bF2c9407fEa4dC236fAbA485A', NULL, NULL, b'0', b'0', b'0', 'Swipe', 'swipe', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/SXP.png');
INSERT INTO `crypto_asset` VALUES (27, 'BAND', '0xAD6cAEb32CD2c308980a548bD0Bc5AA4306c6c18', NULL, NULL, b'0', b'0', b'0', 'Band Protocol', 'band-protocol', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BAND.png');
INSERT INTO `crypto_asset` VALUES (28, 'BUSD', '0x544D0AD6BdDa001630941f6523D935E32cCAb604', NULL, NULL, b'0', b'0', b'1', 'Binance USD', 'binance-usd', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BTCB.png');
INSERT INTO `crypto_asset` VALUES (29, 'DAI', '0xaeeFfB36f4F8f0603AF63607f839D9457fc8b292', NULL, NULL, b'0', b'0', b'1', 'Dai', 'dai', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/DAI.png');
INSERT INTO `crypto_asset` VALUES (30, 'COMP', '0x52CE071Bd9b1C4B00A0b92D298c512478CaD67e8', NULL, NULL, b'0', b'0', b'0', 'Compound', 'compound-governance-token', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/COMP.png');
INSERT INTO `crypto_asset` VALUES (31, 'YFI', '0x88f1A5ae2A3BF98AEAF342D26B30a79438c9142e', NULL, NULL, b'0', b'0', b'0', 'yearn.finance', 'yearn-finance', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/YFI.png');
INSERT INTO `crypto_asset` VALUES (32, 'YFII', '0x7F70642d88cf1C4a3a7abb072B53B929b653edA5', NULL, NULL, b'0', b'0', b'0', 'DFI.Money', 'yfii-finance', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/YFII.png');
INSERT INTO `crypto_asset` VALUES (33, 'NEAR', '0x1Fa4a73a3F0133f0025378af00236f3aBDEE5D63', NULL, NULL, b'0', b'0', b'0', 'NEAR Protocol', 'near', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/NEAR.png');
INSERT INTO `crypto_asset` VALUES (34, 'DOT', '0x62950ba1AdB2C867A24599a3666bf83Cf84c29Ed', NULL, NULL, b'0', b'1', b'0', 'Polkadot', 'polkadot', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/DOT.png');
INSERT INTO `crypto_asset` VALUES (35, 'BEL', '0x8443f091997f06a61670B735ED92734F5628692F', NULL, NULL, b'0', b'0', b'0', 'Bella Protocol', 'bella-protocol', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BEL.png');
INSERT INTO `crypto_asset` VALUES (36, 'UNI', '0xBf5140A22578168FD562DCcF235E5D43A02ce9B1', NULL, NULL, b'0', b'0', b'0', 'Uniswap', 'uniswap', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/UNI.png');
INSERT INTO `crypto_asset` VALUES (37, 'INJ', '0xa2B726B1145A4773F68593CF171187d8EBe4d495', NULL, NULL, b'0', b'0', b'0', 'Injective Protocol', 'injective-protocol', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/INJ.png');
INSERT INTO `crypto_asset` VALUES (38, 'BNB', '0x0000000000000000000000000000000000000000', NULL, NULL, b'0', b'1', b'0', 'Binance Coin', 'binancecoin', 'https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/BNB.png');

-- ----------------------------
-- Table structure for offer
-- ----------------------------
DROP TABLE IF EXISTS `offer`;
CREATE TABLE `offer`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` datetime(0) NOT NULL,
  `duration` int(0) NOT NULL,
  `duration_unit` enum('WEEK','MONTH') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `interest_rate` decimal(19, 2) NOT NULL,
  `liquidation_threshold` decimal(19, 2) NOT NULL,
  `loan_amount` decimal(19, 5) NOT NULL,
  `loan_to_value` decimal(19, 2) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` enum('OPEN','ACCEPTED','REJECTED','CANCELLED') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `collateral_id` bigint(0) NOT NULL,
  `repayment_currency_id` bigint(0) NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_collateral_id`(`collateral_id`) USING BTREE,
  INDEX `fk_repayment_currency_id`(`repayment_currency_id`) USING BTREE,
  CONSTRAINT `fk_collateral_id` FOREIGN KEY (`collateral_id`) REFERENCES `collateral` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_repayment_currency_id` FOREIGN KEY (`repayment_currency_id`) REFERENCES `crypto_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of offer
-- ----------------------------
INSERT INTO `offer` VALUES (1, '2021-10-23 16:39:11', 6, 'MONTH', 70.00, 0.03, 90.00000, 0.01, 'string', 'REJECTED', 12, 1, '00');
INSERT INTO `offer` VALUES (2, '2021-10-23 16:39:43', 6, 'MONTH', 50.00, 0.03, 70.00000, 0.01, 'helloi want to', 'ACCEPTED', 12, 1, '00');
INSERT INTO `offer` VALUES (3, '2021-10-23 23:37:26', 3, 'WEEK', 50.00, 0.02, 70.00000, 0.01, 'offer 1', 'CANCELLED', 9, 1, '00');
INSERT INTO `offer` VALUES (4, '2021-10-23 23:37:37', 3, 'MONTH', 50.00, 0.02, 70.00000, 0.01, 'offer 2', 'CANCELLED', 9, 1, '00');
INSERT INTO `offer` VALUES (10, '2021-10-25 01:34:07', 4, 'MONTH', 50.00, 0.10, 0.00500, 50.00, 'string', 'OPEN', 17, 1, 'string');

SET FOREIGN_KEY_CHECKS = 1;
