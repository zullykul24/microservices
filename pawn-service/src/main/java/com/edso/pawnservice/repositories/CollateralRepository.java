package com.edso.pawnservice.repositories;

import com.edso.pawnservice.domains.Collateral;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CollateralRepository extends JpaRepository<Collateral, Long> ,
        JpaSpecificationExecutor<Collateral>, PagingAndSortingRepository<Collateral, Long> {

    Page<Collateral> findAll(Pageable pageable);

    @Override
    Page<Collateral> findAll(Specification<Collateral> spec, Pageable pageable);
}
