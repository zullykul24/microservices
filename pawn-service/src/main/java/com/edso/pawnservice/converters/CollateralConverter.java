package com.edso.pawnservice.converters;

import com.edso.pawnservice.converters.enum_converters.StringToCollateralStatus;
import com.edso.pawnservice.converters.enum_converters.StringToDurationUnit;
import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.dtos.CollateralDTO;
import org.springframework.stereotype.Component;

@Component
public class CollateralConverter {
    private final StringToDurationUnit stringToDurationUnit;
    private final StringToCollateralStatus stringToCollateralStatus;


    public CollateralConverter(StringToDurationUnit stringToDurationUnit,
                               StringToCollateralStatus stringToCollateralStatus) {
        this.stringToDurationUnit = stringToDurationUnit;
        this.stringToCollateralStatus = stringToCollateralStatus;
    }

    public Collateral toCollateral(CollateralDTO source){
        if(source == null) return null;
        final Collateral collateral = new Collateral();
        collateral.setId(source.getId());
        collateral.setMessage(source.getMessage());
        collateral.setDurationUnit(stringToDurationUnit.convert(source.getDurationUnit()));
        collateral.setAmount(source.getAmount());
        collateral.setDuration(source.getDuration());
        collateral.setWalletAddress(source.getWalletAddress());
        collateral.setOfferReceived(source.getOfferReceived());
        collateral.setStatus(stringToCollateralStatus.convert(source.getStatus()));

        return collateral;
    }

    public CollateralDTO toCollateralDTO(Collateral source){
        if(source == null) return null;
        final CollateralDTO dto = new CollateralDTO();
        dto.setId(source.getId());

        dto.setCurrencyId(source.getCurrency().getId());
        dto.setCurrencyUrl(source.getCurrency().getUrl());
        dto.setCurrencySymbol(source.getCurrency().getSymbol());

        dto.setDurationUnit(source.getDurationUnit().toString());
        dto.setAmount(source.getAmount());
        dto.setDuration(source.getDuration());
        dto.setWalletAddress(source.getWalletAddress());
        dto.setMessage(source.getMessage());
        dto.setStatus(source.getStatus().toString());
        dto.setOfferReceived(source.getOfferReceived());

        dto.setLoanCurrencyId(source.getLoanCurrency().getId());
        dto.setLoanCurrencyUrl(source.getLoanCurrency().getUrl());
        dto.setLoanCurrencySymbol(source.getLoanCurrency().getSymbol());

        return dto;
    }
}
