package com.edso.pawnservice.converters;

import com.edso.pawnservice.domains.Crypto;
import com.edso.pawnservice.dtos.CryptoDTO;
import org.springframework.stereotype.Component;

@Component
public class CryptoConverter {

    public Crypto toCrypto(CryptoDTO source){
        if(source == null) return null;
        Crypto crypto = new Crypto();
        crypto.setId(source.getId());
        crypto.setSymbol(source.getSymbol());
        crypto.setWhitelistCollateral(source.isWhitelistCollateral());
        crypto.setWhitelistSupply(source.isWhitelistSupply());
        crypto.setName(source.getName());
        crypto.setCoinGeckoId(source.getCoinGeckoId());
        crypto.setUrl(source.getUrl());
        return crypto;
    }

    public CryptoDTO toCryptoDTO(Crypto source){
        if(source == null) return null;
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(source.getId());
        cryptoDTO.setSymbol(source.getSymbol());

        cryptoDTO.setWhitelistCollateral(source.isWhitelistCollateral());
        cryptoDTO.setWhitelistSupply(source.isWhitelistSupply());
        cryptoDTO.setName(source.getName());
        cryptoDTO.setCoinGeckoId(source.getCoinGeckoId());
        cryptoDTO.setUrl(source.getUrl());
        return cryptoDTO;
    }
}
