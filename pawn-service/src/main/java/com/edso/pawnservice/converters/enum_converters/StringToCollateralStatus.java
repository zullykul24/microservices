package com.edso.pawnservice.converters.enum_converters;

import com.edso.pawnservice.enums.CollateralStatus;

import com.edso.pawnservice.exceptions.CollateralStatusException;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToCollateralStatus implements Converter<String, CollateralStatus> {
    @Override
    public CollateralStatus convert(String source) {
        try {
            return CollateralStatus.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e){
            throw new CollateralStatusException();
        }
    }
}
