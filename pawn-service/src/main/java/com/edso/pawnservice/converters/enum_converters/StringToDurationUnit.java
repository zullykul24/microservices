package com.edso.pawnservice.converters.enum_converters;

import com.edso.pawnservice.enums.DurationUnit;
import com.edso.pawnservice.exceptions.DurationUnitNotExistException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToDurationUnit implements Converter<String, DurationUnit> {

    @Override
    public DurationUnit convert(String source) {
        try {
            return DurationUnit.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e){
            throw new DurationUnitNotExistException();
        }

    }
}
