package com.edso.pawnservice.converters.enum_converters;


import com.edso.pawnservice.enums.OfferStatus;

import com.edso.pawnservice.exceptions.OfferStatusException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToOfferStatus implements Converter<String, OfferStatus> {
    @Override
    public OfferStatus convert(String source) {
        try {
            return OfferStatus.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e){
            throw new OfferStatusException();
        }
    }
}
