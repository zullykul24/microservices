package com.edso.pawnservice.converters;

import com.edso.pawnservice.converters.enum_converters.StringToDurationUnit;
import com.edso.pawnservice.converters.enum_converters.StringToOfferStatus;
import com.edso.pawnservice.domains.Offer;
import com.edso.pawnservice.dtos.OfferDTO;
import org.springframework.stereotype.Component;

@Component
public class OfferConverter {
    private final StringToOfferStatus stringToOfferStatus;
    private final StringToDurationUnit stringToDurationUnit;

    public OfferConverter(StringToOfferStatus stringToOfferStatus,
                          StringToDurationUnit stringToDurationUnit) {
        this.stringToOfferStatus = stringToOfferStatus;
        this.stringToDurationUnit = stringToDurationUnit;
    }

    public Offer toOffer(OfferDTO source){
        if(source == null) return null;
        final Offer offer = new Offer();
        offer.setId(source.getId());
        offer.setMessage(source.getMessage());
        offer.setAddress(source.getAddress());
        offer.setLoanToValue(source.getLoanToValue());
        offer.setLoanAmount(source.getLoanAmount());
        offer.setInterestRate(source.getInterestRate());
        offer.setLiquidationThreshold(source.getLiquidationThreshold());
        offer.setDuration(source.getDuration());
        offer.setDurationUnit(stringToDurationUnit.convert(source.getDurationUnit()));
        offer.setStatus(stringToOfferStatus.convert(source.getStatus()));
        offer.setCreatedTime(source.getCreatedTime());

        return offer;
    }

    public OfferDTO toOfferDTO(Offer source){
        if(source == null)return null;
        final OfferDTO offerDTO = new OfferDTO();
        offerDTO.setId(source.getId());
        offerDTO.setMessage(source.getMessage());
        offerDTO.setAddress(source.getAddress());
        offerDTO.setLoanToValue(source.getLoanToValue());
        offerDTO.setLoanAmount(source.getLoanAmount());
        offerDTO.setInterestRate(source.getInterestRate());
        offerDTO.setLiquidationThreshold(source.getLiquidationThreshold());
        offerDTO.setDuration(source.getDuration());
        offerDTO.setDurationUnit(source.getDurationUnit().toString());

        offerDTO.setRepaymentCurrencyId(source.getRepaymentCurrency().getId());
        offerDTO.setRepaymentCurrencySymbol(source.getRepaymentCurrency().getSymbol());
        offerDTO.setRepaymentCurrencyUrl(source.getRepaymentCurrency().getUrl());

        offerDTO.setStatus(source.getStatus().toString());
        offerDTO.setCreatedTime(source.getCreatedTime());

        offerDTO.setCollateralId(source.getCollateral().getId());
        offerDTO.setCollateralSymbol(source.getCollateral().getLoanCurrency().getSymbol());
        offerDTO.setCollateralUrl(source.getCollateral().getLoanCurrency().getUrl());

        return offerDTO;
    }
}
