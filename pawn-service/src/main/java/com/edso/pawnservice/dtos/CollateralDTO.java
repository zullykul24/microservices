package com.edso.pawnservice.dtos;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CollateralDTO implements Comparable<CollateralDTO>{

    @Hidden
    private Long id;

    @NotNull(message = "Collateral currency id can not be null")
    private Long currencyId;

    @Hidden
    private String currencyUrl;

    @Hidden
    private String currencySymbol;


    @Positive(message = "Amount must be positive")
    @NotNull(message = "Amount can not be null")
    private BigDecimal amount;

    @Hidden
    private BigDecimal estimateAmount;

    @NotNull(message = "Duration can not be null")
    @Positive(message = "Duration must be positive")
    private Integer duration;

    @NotBlank(message = "Wallet address can not be blank")
    private String walletAddress;

    @NotBlank(message = "Duration unit can not be blank")
    private String durationUnit;

    @Size(max = 100, message = "Message must has length smaller than 100")
    @NotBlank(message = "Message can not be blank")
    private String message;

    @NotNull(message = "Loan currency id can not be null")
    private Long loanCurrencyId;

    @Hidden
    private String loanCurrencyUrl;

    @Hidden
    private String loanCurrencySymbol;

    @Hidden
    private Integer offerReceived;

    @Hidden
    private String status;

    @Override
    public int compareTo(CollateralDTO o) {
        return this.id.compareTo(o.id);
    }
}
