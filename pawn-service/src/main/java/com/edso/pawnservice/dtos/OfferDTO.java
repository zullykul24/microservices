package com.edso.pawnservice.dtos;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferDTO {
    @Hidden
    private Long id;

    @NotBlank(message = "Message can not be blank")
    private String message;

    @NotBlank(message = "Address can not be blank")
    private String address;

    @NotNull(message = "Loan to value can not be null")
    @Positive(message = "Loan to value has to be greater than 0")
    private BigDecimal loanToValue;

    @NotNull(message = "Loan amount can not be null")
    @Positive(message = "Loan amount has to be greater than 0")
    private BigDecimal loanAmount;

    @NotNull(message = "Loan amount can not be null")
    @PositiveOrZero(message = "Loan amount has to be greater or equal 0")
    private BigDecimal interestRate;

    @NotNull(message = "Liquidation threshold can not be null")
    @Positive(message = "Liquidation threshold has to be greater than 0")
    private BigDecimal liquidationThreshold;

    @NotNull(message = "Duration can not be null")
    @Positive(message = "Duration has to be greater than 0")
    private Integer duration;

    @NotBlank(message = "Duration unit can not be blank")
    private String durationUnit;

    @NotNull(message = "Repayment currency id can not be null")
    private Long repaymentCurrencyId;

    @Hidden
    private String repaymentCurrencySymbol;

    @Hidden
    private String repaymentCurrencyUrl;

    @Hidden
    private String status;

    @Hidden
    private Timestamp createdTime;

    @NotNull(message = "Collateral currency id can not be null")
    private Long collateralId;

    @Hidden
    private String collateralSymbol;

    @Hidden
    private String collateralUrl;


}
