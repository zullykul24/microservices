package com.edso.pawnservice.exceptions;

public class CollateralStatusException extends RuntimeException {
    public CollateralStatusException(){}

    public CollateralStatusException(String message){
        super(message);
    }
}
