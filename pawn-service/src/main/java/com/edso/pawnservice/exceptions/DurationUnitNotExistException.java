package com.edso.pawnservice.exceptions;

public class DurationUnitNotExistException extends RuntimeException{
    public DurationUnitNotExistException(){}

    public DurationUnitNotExistException(String message){
        super(message);
    }
}
