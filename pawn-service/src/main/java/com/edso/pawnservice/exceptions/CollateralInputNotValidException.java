package com.edso.pawnservice.exceptions;

public class CollateralInputNotValidException extends RuntimeException{
    public CollateralInputNotValidException(){}

    public CollateralInputNotValidException(String message){
        super(message);
    }


}
