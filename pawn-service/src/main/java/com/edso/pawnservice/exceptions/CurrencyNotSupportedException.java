package com.edso.pawnservice.exceptions;

public class CurrencyNotSupportedException extends RuntimeException{
    public CurrencyNotSupportedException(){}

    public CurrencyNotSupportedException(String message){
        super(message);
    }
}
