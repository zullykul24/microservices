package com.edso.pawnservice.exceptions;

public class OfferInputNotValidException extends RuntimeException {
    public OfferInputNotValidException(String message) {
        super(message);
    }
}
