package com.edso.pawnservice.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "crypto_asset")
@Data
@NoArgsConstructor
public class Crypto implements Serializable {

    private final static long serialVersionUID = 270727596527329665L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String symbol;

    private boolean whitelistCollateral;
    private boolean whitelistSupply;
    private String name;
    private String coinGeckoId;
    private String url;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "currency", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Collateral> collateralCurrencyList = new ArrayList<>();

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "loanCurrency", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Collateral> collateralLoanCurrencyList = new ArrayList<>();

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "repaymentCurrency", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Offer> offerRepaymentCurrencyList = new ArrayList<>();


    @Override
    public int hashCode() {
        return symbol != null ? symbol.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Crypto{" +
                "id=" + id +
                ", symbol='" + symbol + '\'' +
                ", name='" + name + '\'' +
                ", coinGeckoId='" + coinGeckoId + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
