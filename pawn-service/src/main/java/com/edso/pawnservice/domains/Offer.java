package com.edso.pawnservice.domains;

import com.edso.pawnservice.enums.DurationUnit;
import com.edso.pawnservice.enums.OfferStatus;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

import java.sql.Timestamp;

@Entity
@Data
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String message;

    private String address;

    private BigDecimal loanToValue;

    private BigDecimal loanAmount;

    private BigDecimal interestRate;

    private BigDecimal liquidationThreshold;

    private Integer duration;

    @Enumerated(EnumType.STRING)
    private DurationUnit durationUnit;


    @ManyToOne
    @JoinColumn(name = "repayment_currency_id", foreignKey = @ForeignKey(name = "fk_repayment_currency_id"))
    private Crypto repaymentCurrency;

    @Enumerated(EnumType.STRING)
    private OfferStatus status;

    private Timestamp createdTime;

    @ManyToOne
    @JoinColumn(name = "collateral_id", foreignKey = @ForeignKey(name = "fk_collateral_id"))
    private Collateral collateral;
}
