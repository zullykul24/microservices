package com.edso.pawnservice.domains;

import com.edso.pawnservice.enums.CollateralStatus;
import com.edso.pawnservice.enums.DurationUnit;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

import java.math.BigDecimal;

import java.util.List;

@Entity
@Data
@Table(name = "collateral")
public class Collateral {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    ///change to Crypto
    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "fk_currency_id"))
    private Crypto currency;


    private BigDecimal amount;


    private String walletAddress;


    private Integer duration;

    @Enumerated(EnumType.STRING)
    private DurationUnit durationUnit;


    private String message;


    ///change to Crypto
    @ManyToOne
    @JoinColumn(name = "loan_currency_id", foreignKey = @ForeignKey(name = "fk_loan_currency_id"))
    private Crypto loanCurrency;

    private Integer offerReceived;

    @Enumerated(EnumType.STRING)
    private CollateralStatus status;


    @OneToMany(mappedBy = "collateral", cascade = CascadeType.ALL)
    private List<Offer> offers;
}


