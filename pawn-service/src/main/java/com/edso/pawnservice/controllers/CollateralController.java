package com.edso.pawnservice.controllers;

import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.dtos.CollateralDTO;

import com.edso.pawnservice.dtos.CollateralDetailsDTO;
import com.edso.pawnservice.repositories.CollateralRepository;
import com.edso.pawnservice.services.CollateralService;
import com.edso.pawnservice.services.CryptoService;

import com.edso.pawnservice.services.OfferService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("pawn/collaterals")
public class CollateralController {

    private final CryptoService cryptoService;
    private final CollateralRepository collateralRepository;
    private final CollateralService collateralService;
    private final OfferService offerService;

    public CollateralController(CryptoService cryptoService,
                                CollateralRepository collateralRepository,
                                CollateralService collateralService,
                                OfferService offerService) {
        this.cryptoService = cryptoService;
        this.collateralRepository = collateralRepository;
        this.collateralService = collateralService;
        this.offerService = offerService;
    }


    @Operation(summary = "Create new collateral")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CollateralDTO createNewCollateral(@Valid @RequestBody CollateralDTO collateralDTO)  {
        return collateralService.createNewCollateral(collateralDTO);
    }

//    @Operation(summary = "Get all collateral")
//    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
//    public List<Collateral> getCollateralList(){
//        return collateralRepository.findAll();
//    }

    @Operation(summary = "Find by multi props")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CollateralDTO> getCollateralByProps(
        @RequestParam(required = false) String walletAddress,
        @RequestParam(required = false) String collateralCurrency,
        @RequestParam(required = false) String loanCurrency,
        @Parameter(description = "Collateral status: Open, Accepted, Withdrawn")
        @RequestParam(required = false) String status,
        @RequestParam(defaultValue = "1") Long page,
        @RequestParam(defaultValue = "5") Integer pageSize){
        return collateralService.findCollateralSpecification(
                walletAddress,
                collateralCurrency,
                loanCurrency,
                status,
                page,
                pageSize);
    }

    @Operation(summary = "Withdraw a collateral")
    @PatchMapping({"/withdraw"})
    @ResponseStatus(HttpStatus.OK)
    public CollateralDTO withdrawCollateral(@RequestParam Long id){
        return collateralService.withdraw(id);
    }

    @Operation(summary = "Accept an offer")
    @PatchMapping("/{collateral_id}/accept/{offer_id}")
    @ResponseStatus(HttpStatus.OK)
    public CollateralDTO acceptOffer(@PathVariable Long collateral_id,
                                     @PathVariable Long offer_id){
        return collateralService.acceptOffer(collateral_id, offer_id);
    }

    @Operation(summary = "Get collateral details")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CollateralDetailsDTO getCollateralDetails(@PathVariable Long id,
                                                     @RequestParam Long page,
                                                     @RequestParam Integer pageSize){
        return new CollateralDetailsDTO(collateralService.getDetailsById(id),
                offerService.getOfferListByCollateralId(id, page, pageSize));
    }



}
