package com.edso.pawnservice.controllers;


import com.edso.pawnservice.exceptions.*;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

import org.springframework.validation.FieldError;

import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(Exception e, WebRequest request){
        return new ResponseEntity<>("Currency not found", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({DurationUnitNotExistException.class})
    public ResponseEntity<Object> handleDurationUnitNotExistException(Exception e, WebRequest request){
        return new ResponseEntity<>("Duration unit invalid", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({CurrencyNotSupportedException.class})
    public ResponseEntity<Object> handleCurrencyNotSupportedException(Exception e, WebRequest request){
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({CollateralInputNotValidException.class})
    public ResponseEntity<Object> handleCollateralInputNotValidException(Exception e, WebRequest request){
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({CollateralStatusException.class})
    public ResponseEntity<Object> handleCollateralStatusException(Exception e, WebRequest request){
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e, WebRequest request){
        List<String> errorMessages = e.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(errorMessages.toString());
    }

//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//        BindingResult result = ex.getBindingResult();
//        List<FieldError> fieldErrors = result.getFieldErrors().stream()
//                .map(f -> new FieldError(f.getObjectName(), f.getField(), f.getCode()))
//                .collect(Collectors.toList());
//
//        List<String> fieldsAndRejectedValues = fieldErrors
//                .stream()
//                .map(fieldError -> fieldError.getField() + " is " + fieldError.getRejectedValue())
//                .collect(Collectors.toList());
//        return handleExceptionInternal(ex, "Method argument not valid, fieldErrors:\n" + fieldsAndRejectedValues,
//                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
//    }


//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(
//            MethodArgumentNotValidException ex,
//            HttpHeaders headers,
//            HttpStatus status,
//            WebRequest request) {
//        List<String> errors = new ArrayList<String>();
//        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
//            errors.add(error.getField() + ": " + error.getDefaultMessage());
//        }
//        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
//            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
//        }
//
//        ApiError apiError =
//                new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
////        return handleExceptionInternal(
////                ex, apiError, headers, apiError.getStatus(), request);
//        return ResponseEntity.badRequest().body(
//                ex.getBindingResult()
//                        .getFieldErrors()
//                        .stream()
//                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
//                        .collect(Collectors.toList())
//                        .toString()
//        );
//    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) ->{

            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
