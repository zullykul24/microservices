package com.edso.pawnservice.controllers;

import com.edso.pawnservice.dtos.CryptoDTO;
import com.edso.pawnservice.services.CryptoService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"pawn/cryptos/"})
public class CryptoController {
    private final CryptoService cryptoService;

    public CryptoController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @Operation(summary = "Get all cryptos that can be used as collateral")
    @GetMapping({"/collateral_cryptos"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoDTO> getCollateralCryptos(){
        return cryptoService.getCollateralCryptos();
    }

    @Operation(summary = "Get all cryptos that can be supplied")
    @GetMapping({"/supply_cryptos"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoDTO> getSupplyCryptos(){
        return cryptoService.getSupplyCryptos();
    }

    @Operation(summary = "Find by id")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CryptoDTO getCryptoById(@PathVariable Long id){
        return cryptoService.getCryptoById(id);

    }
}
