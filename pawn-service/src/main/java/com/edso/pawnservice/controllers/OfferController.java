package com.edso.pawnservice.controllers;

import com.edso.pawnservice.dtos.CollateralDTO;
import com.edso.pawnservice.dtos.OfferDTO;
import com.edso.pawnservice.repositories.OfferRepository;
import com.edso.pawnservice.services.OfferService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping({"pawn/offers"})
public class OfferController {
    private final OfferService offerService;
    private final OfferRepository offerRepository;

    public OfferController(OfferService offerService, OfferRepository offerRepository) {
        this.offerService = offerService;
        this.offerRepository = offerRepository;
    }

    @Operation(summary = "Create new offer")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OfferDTO createNewOffer(@Valid @RequestBody OfferDTO offerDTO){
        return offerService.createNewOffer(offerDTO);
    }

    @Operation(summary = "Get offer details")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OfferDTO getOfferDetails(@PathVariable Long id){
        return offerService.showDetailsById(id);
    }

    @Operation(summary = "Get offer list by collateral")
    @GetMapping("/offers_by_collateral/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<OfferDTO> getOfferListByCollateral(@PathVariable Long id,
                                                   @RequestParam Long page,
                                                   @RequestParam Integer pageSize){
        return offerService.getOfferListByCollateralId(id, page, pageSize);
    }

}
