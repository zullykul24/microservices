package com.edso.pawnservice.specifications;

import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.domains.Collateral_;

import com.edso.pawnservice.enums.CollateralStatus;
import org.springframework.data.jpa.domain.Specification;

public class CollateralSpecification {

    public static Specification<Collateral> hasWalletAddress(String walletAddress) {
        return (root, query, cb) -> cb.equal(root.get(Collateral_.WALLET_ADDRESS), walletAddress);
    }

    public static Specification<Collateral> hasCollateralCurrency(String collateralCurrency) {
        return (root, query, cb) -> cb.equal(root.get("currency").get("symbol"), collateralCurrency);
    }

    public static Specification<Collateral> hasLoanCurrency(String loanCurrency) {
        return (root, query, cb) -> cb.equal(root.get("loanCurrency").get("symbol"), loanCurrency);
    }


    public static Specification<Collateral> hasStatus(CollateralStatus status) {
        return (root, query, cb) -> cb.equal(root.get(Collateral_.STATUS), status);
    }
}
