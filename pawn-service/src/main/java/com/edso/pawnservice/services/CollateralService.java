package com.edso.pawnservice.services;


import com.edso.pawnservice.dtos.CollateralDTO;

import java.util.List;

public interface CollateralService {
    CollateralDTO createNewCollateral(CollateralDTO collateralDTO);

    CollateralDTO findById(Long id);

    CollateralDTO receiveOffer(Long id);

    CollateralDTO withdraw(Long id);

    CollateralDTO getDetailsById(Long id);



    CollateralDTO acceptOffer(Long collateralId, Long offerId);



    List<CollateralDTO> findCollateralSpecification(String walletAddress,
                                                    String collateralCurrency,
                                                    String loanCurrency,
                                                    String status,
                                                    Long page,
                                                    Integer pageSize);
}
