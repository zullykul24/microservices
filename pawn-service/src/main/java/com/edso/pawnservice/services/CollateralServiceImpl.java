package com.edso.pawnservice.services;

import com.edso.pawnservice.converters.CollateralConverter;
import com.edso.pawnservice.converters.CryptoConverter;
import com.edso.pawnservice.converters.enum_converters.StringToCollateralStatus;
import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.dtos.CollateralDTO;
import com.edso.pawnservice.dtos.CryptoDTO;


import com.edso.pawnservice.enums.CollateralStatus;
import com.edso.pawnservice.enums.DurationUnit;
import com.edso.pawnservice.exceptions.CollateralInputNotValidException;
import com.edso.pawnservice.exceptions.CollateralStatusException;
import com.edso.pawnservice.exceptions.CurrencyNotSupportedException;
import com.edso.pawnservice.exceptions.ResourceNotFoundException;
import com.edso.pawnservice.repositories.CollateralRepository;
import com.edso.pawnservice.specifications.CollateralSpecification;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CollateralServiceImpl implements CollateralService {
    private final CollateralRepository collateralRepository;
    private final CollateralConverter collateralConverter;
    private final CryptoConverter cryptoConverter;
    private final CryptoService cryptoService;
    private final StringToCollateralStatus stringToCollateralStatus;
    private final OfferService offerService;
    private final RestTemplate restTemplate;


    public CollateralServiceImpl(CollateralRepository collateralRepository,
                                 CollateralConverter collateralConverter,
                                 CryptoConverter cryptoConverter,
                                 CryptoService cryptoService,
                                 StringToCollateralStatus stringToCollateralStatus,
                                 @Lazy OfferService offerService,
                                 RestTemplate restTemplate) {
        this.collateralRepository = collateralRepository;
        this.collateralConverter = collateralConverter;
        this.cryptoConverter = cryptoConverter;
        this.cryptoService = cryptoService;
        this.stringToCollateralStatus = stringToCollateralStatus;
        this.offerService = offerService;
        this.restTemplate = restTemplate;
    }

    @Override
    public CollateralDTO createNewCollateral(CollateralDTO collateralDTO) {
        collateralDTO.setStatus(CollateralStatus.OPEN.toString());
        collateralDTO.setOfferReceived(0);

        if(collateralDTO.getDurationUnit().equalsIgnoreCase(DurationUnit.WEEK.toString())){
            if (collateralDTO.getDuration() > 5200) {
                throw new CollateralInputNotValidException("Duration by week can not greater than 5200");
            }
        }

        if(collateralDTO.getDurationUnit().equalsIgnoreCase(DurationUnit.MONTH.toString())){
            if (collateralDTO.getDuration() > 1200) {
                throw new CollateralInputNotValidException("Duration by month can not greater than 1200");
            }
        }

        Collateral collateral = collateralConverter.toCollateral(collateralDTO);

        CryptoDTO cryptoDTOCurrency = cryptoService.getCryptoById(collateralDTO.getCurrencyId());
        CryptoDTO cryptoDTOLoanCurrency = cryptoService.getCryptoById(collateralDTO.getLoanCurrencyId());


        //check if currencies are supported to be used as collateral or loan-currency
        if(!cryptoDTOCurrency.isWhitelistCollateral()) {
            throw new CurrencyNotSupportedException(cryptoDTOCurrency.getSymbol().toUpperCase() +
                    " is not supported to be used as collateral.");
        }

        if(!cryptoDTOLoanCurrency.isWhitelistSupply()){
            throw new CurrencyNotSupportedException(cryptoDTOLoanCurrency.getSymbol().toUpperCase() +
                    " is not supported to be used as loan currency.");
        }


        collateral.setCurrency(cryptoConverter.toCrypto(cryptoDTOCurrency));
        collateral.setLoanCurrency(cryptoConverter.toCrypto(cryptoDTOLoanCurrency));


        Collateral savedCollateral = collateralRepository.save(collateral);

        return collateralConverter.toCollateralDTO(savedCollateral);
    }

    @Override
    public CollateralDTO findById(Long id) {
        Collateral collateral = collateralRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        return collateralConverter.toCollateralDTO(collateral);
    }

    @Override
    public CollateralDTO receiveOffer(Long id) {
        Collateral collateral = collateralRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        //receive a new offer then increase offers received by 1
        collateral.setOfferReceived(collateral.getOfferReceived() + 1);

        return collateralConverter.toCollateralDTO(collateral);
    }

    @Override
    @Transactional
    public CollateralDTO withdraw(Long id) {
        Collateral collateral = collateralRepository.findById(id).get();

        if(!collateral.getStatus().equals(CollateralStatus.OPEN)){
            throw new CollateralStatusException("This collateral is not open");
        }
        offerService.cancelOffer(id);

        collateral.setStatus(CollateralStatus.WITHDRAWN);
        Collateral savedCollateral = collateralRepository.save(collateral);
        return collateralConverter.toCollateralDTO(savedCollateral);

    }

    @Override
    public CollateralDTO getDetailsById(Long id) {
        Collateral collateral = collateralRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        //get estimate
        String collateralSymbol = collateral.getCurrency().getSymbol();

        BigDecimal rate = restTemplate
                .getForObject("http://CRYPTO-SERVICE/coin-gecko/price?symbol=" +
                        collateralSymbol, BigDecimal.class);

        System.out.println("rate = " + rate);


        BigDecimal estimateAmount = collateral.getAmount().multiply(rate);

        BigDecimal roundedEstimateAmount = estimateAmount.setScale(2, RoundingMode.HALF_UP);

        CollateralDTO collateralDTO = collateralConverter.toCollateralDTO(collateral);
        collateralDTO.setEstimateAmount(roundedEstimateAmount);


        return collateralDTO;
        //http://localhost:9001/coin_gecko/price?symbol=btc
        //http://localhost:9191/coin_gecko/price?symbol=btc
        //http://localhost:9001/coin_gecko/price?symbol=btc

    }



    @Override
    @Transactional
    public CollateralDTO acceptOffer(Long collateralId, Long offerId) {
        CollateralDTO collateralDTO = findById(collateralId);
        System.out.println(collateralDTO.getCurrencyId());
        if(!collateralDTO.getStatus().equalsIgnoreCase(CollateralStatus.OPEN.toString())){
            throw new CollateralStatusException("You can only accept an offer when collateral is open");
        }

        collateralDTO.setStatus(CollateralStatus.ACCEPTED.toString());
        offerService.acceptOffer(collateralId, offerId);
        Collateral collateral = collateralConverter.toCollateral(collateralDTO);

        //set Currency and LoanCurrency
        //bad code
        collateral.setCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(collateralDTO.getCurrencyId())));
        collateral.setLoanCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(collateralDTO.getLoanCurrencyId())));
        Collateral savedCollateral = collateralRepository.save(collateral);

        return collateralConverter.toCollateralDTO(savedCollateral);

    }


    private Page<Collateral> findAllByProps(String walletAddress,
                                            String collateralCurrency,
                                            String loanCurrency,
                                            String status,
                                            Long page,
                                            Integer pageSize){
        Specification condition = Specification.where(null);

        if(walletAddress != null){
            condition = condition.and(CollateralSpecification.hasWalletAddress(walletAddress));
        }

        if(collateralCurrency != null){
            condition = condition.and(CollateralSpecification.hasCollateralCurrency(collateralCurrency));
        }

        if(loanCurrency != null){
            condition = condition.and(CollateralSpecification.hasLoanCurrency(loanCurrency));
        }

        if(status != null){
            condition = condition.and(CollateralSpecification.hasStatus(stringToCollateralStatus.convert(status)));
        }

        return collateralRepository
                .findAll(condition, PageRequest.of(Math.toIntExact(page - 1),pageSize, Sort.by("id")));


    }

    @Override
    public List<CollateralDTO> findCollateralSpecification(String walletAddress,
                                                           String collateralCurrency,
                                                           String loanCurrency,
                                                           String status,
                                                           Long page,
                                                           Integer pageSize) {
        List<CollateralDTO> list = findAllByProps(
                walletAddress,
                collateralCurrency,
                loanCurrency,
                status,
                page,
                pageSize
        )
        .stream()
        .map(collateralConverter::toCollateralDTO)
        .collect(Collectors.toList());

        Collections.sort(list);
        if(list.isEmpty()) throw new ResourceNotFoundException();
        return list;
    }
}
