package com.edso.pawnservice.services;

import com.edso.pawnservice.converters.CryptoConverter;
import com.edso.pawnservice.domains.Crypto;
import com.edso.pawnservice.dtos.CryptoDTO;
import com.edso.pawnservice.exceptions.ResourceNotFoundException;
import com.edso.pawnservice.repositories.CryptoRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CryptoServiceImpl implements CryptoService {

    private final CryptoRepository cryptoRepository;
    private final CryptoConverter cryptoConverter;

    public CryptoServiceImpl(CryptoRepository cryptoRepository,
                             CryptoConverter cryptoConverter) {
        this.cryptoRepository = cryptoRepository;
        this.cryptoConverter = cryptoConverter;
    }



    @Override
    public List<CryptoDTO> getAllCryptos() {
        List<CryptoDTO> list = cryptoRepository.findAll()
                .stream().map(cryptoConverter::toCryptoDTO)
                .collect(Collectors.toList());
        Collections.sort(list);
        return list;
    }

    @Override
    public List<CryptoDTO> getCollateralCryptos() {
        List<CryptoDTO> list = cryptoRepository.findAll()
                .stream()
                .filter(crypto -> crypto.isWhitelistCollateral())
                .map(cryptoConverter::toCryptoDTO)
                .collect(Collectors.toList());
        Collections.sort(list);
        return list;
    }

    @Override
    public List<CryptoDTO> getSupplyCryptos() {
        List<CryptoDTO> list = cryptoRepository.findAll()
                .stream()
                .filter(crypto -> crypto.isWhitelistSupply())
                .map(cryptoConverter::toCryptoDTO)
                .collect(Collectors.toList());
        Collections.sort(list);
        return list;
    }

    @Override
    public CryptoDTO getCryptoById(Long id) {
        Crypto crypto = cryptoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        return cryptoConverter.toCryptoDTO(crypto);
    }


}
