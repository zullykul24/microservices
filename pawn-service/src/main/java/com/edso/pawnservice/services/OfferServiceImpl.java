package com.edso.pawnservice.services;


import com.edso.pawnservice.converters.CollateralConverter;
import com.edso.pawnservice.converters.CryptoConverter;
import com.edso.pawnservice.converters.OfferConverter;
import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.domains.Offer;
import com.edso.pawnservice.dtos.CollateralDTO;
import com.edso.pawnservice.dtos.CryptoDTO;
import com.edso.pawnservice.dtos.OfferDTO;
import com.edso.pawnservice.enums.CollateralStatus;
import com.edso.pawnservice.enums.DurationUnit;
import com.edso.pawnservice.enums.OfferStatus;
import com.edso.pawnservice.exceptions.*;

import com.edso.pawnservice.repositories.OfferRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OfferServiceImpl implements OfferService {
    private final OfferRepository offerRepository;
    private final OfferConverter offerConverter;
    private final CryptoService cryptoService;
    private final CryptoConverter cryptoConverter;
    private final CollateralService collateralService;
    private final CollateralConverter collateralConverter;


    public OfferServiceImpl(OfferRepository offerRepository,
                            OfferConverter offerConverter,
                            CryptoService cryptoService,
                            CryptoConverter cryptoConverter,
                            CollateralService collateralService,
                            CollateralConverter collateralConverter) {
        this.offerRepository = offerRepository;
        this.offerConverter = offerConverter;
        this.cryptoService = cryptoService;
        this.cryptoConverter = cryptoConverter;
        this.collateralService = collateralService;
        this.collateralConverter = collateralConverter;
    }


    @Override
    @Transactional
    public OfferDTO createNewOffer(OfferDTO offerDTO) {
        offerDTO.setCreatedTime(new Timestamp(System.currentTimeMillis()));
        offerDTO.setStatus(OfferStatus.OPEN.toString());

        if(offerDTO.getDurationUnit().equalsIgnoreCase(DurationUnit.WEEK.toString())){
            if (offerDTO.getDuration() > 5200) {
                throw new OfferInputNotValidException("Duration by week can not greater than 5200");
            }
        }

        if(offerDTO.getDurationUnit().equalsIgnoreCase(DurationUnit.MONTH.toString())){
            if (offerDTO.getDuration() > 1200) {
                throw new OfferInputNotValidException("Duration by month can not greater than 1200");
            }
        }
        Offer offer = offerConverter.toOffer(offerDTO);

        //set repayment currency
        CryptoDTO repaymentCurrencyDTO = cryptoService.getCryptoById(offerDTO.getRepaymentCurrencyId());


        //check if currencies are supported to be used as collateral or loan-currency
        if(!repaymentCurrencyDTO.isWhitelistSupply()){
            throw new CurrencyNotSupportedException(repaymentCurrencyDTO.getSymbol().toUpperCase() +
                    " is not supported to be used as loan currency.");
        }

        offer.setRepaymentCurrency(cryptoConverter.toCrypto(repaymentCurrencyDTO));


        //set collateral
        CollateralDTO collateralDTO = collateralService.findById(offerDTO.getCollateralId());

        //check if collateral is open
        if(!collateralDTO.getStatus().equalsIgnoreCase(CollateralStatus.OPEN.toString())){
            throw new CollateralStatusException("You can only send offer to open collateral");
        }

        System.out.println("offer received = " + collateralDTO.getOfferReceived());
        Collateral collateral = collateralConverter.toCollateral(collateralDTO);

        collateral.setCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(collateralDTO.getCurrencyId())));
        collateral.setLoanCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(collateralDTO.getLoanCurrencyId())));

        offer.setCollateral(collateral);

        //save offer
        Offer savedOffer = offerRepository.save(offer);
        if(savedOffer != null){
            collateralService.receiveOffer(offerDTO.getCollateralId());
        }

        return offerConverter.toOfferDTO(savedOffer);
    }

    @Override
    public OfferDTO findById(Long id) {
        Offer offer = offerRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

        return offerConverter.toOfferDTO(offer);
    }

    @Override
    public OfferDTO showDetailsById(Long id) {
        Offer offer = offerRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

        return offerConverter.toOfferDTO(offer);
    }

    @Override
    public List<OfferDTO> getOfferListByCollateralId(Long id) {
        return offerRepository
                .findAll()
                .stream()
                .filter(offer -> offer.getCollateral().getId().equals(id))
                .map(offerConverter::toOfferDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<OfferDTO> getOfferListByCollateralId(Long id, Long page, Integer pageSize) {
        return offerRepository
                .findAll(PageRequest.of(Math.toIntExact(page - 1), pageSize))
                .stream()
                .filter(offer -> offer.getCollateral().getId().equals(id))
                .map(offerConverter::toOfferDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void acceptOffer(Long collateralId, Long offerId) {
        boolean collateralHasOfferId = false;
        List<OfferDTO> offerDTOList = getOfferListByCollateralId(collateralId);

        if(offerDTOList.isEmpty()){
            throw new CollateralStatusException("Can not accept this offer");
        }

        //check if collateral has offerId
        for(OfferDTO offerDTO : offerDTOList){
            if(offerDTO.getId().equals(offerId)){
                collateralHasOfferId = true;
                break;
            }
        }

        if (!collateralHasOfferId){
            throw new CollateralStatusException("Collateral hasn't received this offer");
        }

        for(OfferDTO offerDTO : offerDTOList){
            if (offerDTO.getId().equals(offerId)){
                offerDTO.setStatus(OfferStatus.ACCEPTED.toString());
            } else {
                offerDTO.setStatus(OfferStatus.REJECTED.toString());
            }

            Offer offer = offerConverter.toOffer(offerDTO);

            //set object reference
            offer.setRepaymentCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(offerDTO.getRepaymentCurrencyId())));
            offer.setCollateral(collateralConverter.toCollateral(collateralService.findById(offerDTO.getCollateralId())));
            Offer savedOffer = offerRepository.save(offer);
        }
    }

    @Override
    public void cancelOffer(Long collateralId) {
        List<OfferDTO> offerDTOList = getOfferListByCollateralId(collateralId);

        if(offerDTOList.isEmpty()){
            return;
        }

        //change offer status to Cancelled
        for (OfferDTO offerDTO: offerDTOList){
            offerDTO.setStatus(OfferStatus.CANCELLED.toString());

            Offer offer = offerConverter.toOffer(offerDTO);
            offer.setRepaymentCurrency(cryptoConverter.toCrypto(cryptoService.getCryptoById(offerDTO.getRepaymentCurrencyId())));
            offer.setCollateral(collateralConverter.toCollateral(collateralService.findById(offerDTO.getCollateralId())));
            Offer savedOffer = offerRepository.save(offer);
        }
    }

}
