package com.edso.pawnservice.services;

import com.edso.pawnservice.dtos.CryptoDTO;

import java.util.List;

public interface CryptoService {


    List<CryptoDTO> getAllCryptos();

    List<CryptoDTO> getCollateralCryptos();

    List<CryptoDTO> getSupplyCryptos();

    CryptoDTO getCryptoById(Long id);



}
