package com.edso.pawnservice.services;

import com.edso.pawnservice.dtos.OfferDTO;


import java.util.List;


public interface OfferService {
    OfferDTO createNewOffer(OfferDTO offerDTO);

    OfferDTO findById(Long id);

    OfferDTO showDetailsById(Long id);

    List<OfferDTO> getOfferListByCollateralId(Long id);

    List<OfferDTO> getOfferListByCollateralId(Long id, Long page, Integer pageSize);

    void acceptOffer(Long collateralId, Long offerId);

    void cancelOffer(Long collateralId);


}
