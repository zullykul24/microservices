package com.edso.pawnservice.enums;

public enum OfferStatus {
    OPEN,
    ACCEPTED,
    REJECTED,
    CANCELLED
}
