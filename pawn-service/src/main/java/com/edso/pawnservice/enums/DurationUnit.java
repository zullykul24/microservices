package com.edso.pawnservice.enums;

public enum DurationUnit {
    WEEK,
    MONTH
}
