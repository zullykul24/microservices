package com.edso.pawnservice.enums;

public enum CollateralStatus {
    ACCEPTED,
    OPEN,
    WITHDRAWN
}
