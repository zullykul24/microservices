package com.edso.pawnservice.controllers;

import com.edso.pawnservice.domains.Collateral;
import com.edso.pawnservice.dtos.CollateralDTO;
import com.edso.pawnservice.enums.CollateralStatus;
import com.edso.pawnservice.enums.DurationUnit;
import com.edso.pawnservice.repositories.CollateralRepository;
import com.edso.pawnservice.services.CollateralService;
import com.edso.pawnservice.services.CryptoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CollateralController.class)
public class CollateralControllerTest{

    @Autowired
    MockMvc mockMvc;


    @MockBean
    CollateralService collateralService;

    @MockBean
    CollateralRepository collateralRepository;

    @MockBean
    CryptoService cryptoService;



    @Test
    public void createNewCollateral() throws Exception {
        CollateralDTO mockDto = new CollateralDTO();
        mockDto.setId(1L);
        mockDto.setCurrencyId(1L);
        mockDto.setAmount(new BigDecimal(0.001));
        mockDto.setDuration(3);
        mockDto.setWalletAddress("0x00");
        mockDto.setDurationUnit("WEEK");
        mockDto.setMessage("string");
        mockDto.setLoanCurrencyId(5L);

        String inputInJson = this.mapToJson(mockDto);

        String uri = "/pawn/collaterals";

        Mockito.when(collateralService.createNewCollateral(Mockito.any(CollateralDTO.class))).thenReturn(mockDto);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(uri)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertEquals(outputInJson,inputInJson);
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());


        /**
         *
         * "id": 16,
         *   "currencyId": 1,
         *   "currencyUrl": "https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/DFY.png",
         *   "currencySymbol": "DFY",
         *   "amount": 0.001,
         *   "estimateAmount": null,
         *   "duration": 3,
         *   "walletAddress": "0x00",
         *   "durationUnit": "WEEK",
         *   "message": "string",
         *   "loanCurrencyId": 5,
         *   "loanCurrencyUrl": "https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/USDT.png",
         *   "loanCurrencySymbol": "DFY",
         *   "offerReceived": 0,
         *   "status": "OPEN"*/


    }

    @Test
    public void createNewCollateralWithoutWalletAddress() throws Exception{
        CollateralDTO mockDto = new CollateralDTO();
        mockDto.setId(1L);
        mockDto.setCurrencyId(1L);
        mockDto.setAmount(new BigDecimal(0.001));
        mockDto.setDuration(3);
        mockDto.setDurationUnit("WEEK");
        mockDto.setMessage("string");
        mockDto.setLoanCurrencyId(5L);

        String inputInJson = this.mapToJson(mockDto);

        String uri = "/pawn/collaterals";

        Mockito.when(collateralService.createNewCollateral(Mockito.any(CollateralDTO.class))).thenReturn(mockDto);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(uri)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        String responseWalletAddressBlank =
        "{\"walletAddress\":\"Wallet address can not be blank\"}";

        assertNotEquals(outputInJson,inputInJson);
        assertEquals(outputInJson, responseWalletAddressBlank);
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @org.junit.Test
    public void getCollateralList() throws Exception {

    }

    @org.junit.Test
    public void getCollateralByProps() {
    }

    @Test
    public void withdrawCollateral() throws Exception {
        CollateralDTO mockDto = new CollateralDTO();
        mockDto.setId(1L);
        mockDto.setCurrencyId(1L);
        mockDto.setAmount(new BigDecimal(0.001));
        mockDto.setDuration(3);
        mockDto.setWalletAddress("0x11");
        mockDto.setDurationUnit("WEEK");
        mockDto.setMessage("string");
        mockDto.setLoanCurrencyId(5L);
        mockDto.setStatus("OPEN");

        String mockDtoJson = this.mapToJson(mockDto);

        String uri = "/pawn/collaterals/withdraw";

        Mockito.when(collateralService.withdraw(Mockito.any(Long.class))).thenReturn(mockDto);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .patch(uri)
                .accept(MediaType.APPLICATION_JSON)
                .param("id", "1")
                .content("{}")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertEquals(outputInJson, mockDtoJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @org.junit.Test
    public void acceptOffer() {
    }

    @org.junit.Test
    public void getCollateralDetails() {

    }

    /**
     * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
     */
    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}