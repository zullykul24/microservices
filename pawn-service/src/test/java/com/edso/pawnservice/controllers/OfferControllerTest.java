package com.edso.pawnservice.controllers;



import com.edso.pawnservice.dtos.OfferDTO;

import com.edso.pawnservice.repositories.OfferRepository;

import com.edso.pawnservice.services.CryptoService;
import com.edso.pawnservice.services.OfferService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;


import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@RunWith(SpringRunner.class)
@WebMvcTest(value = OfferController.class)
public class OfferControllerTest {

    @Autowired
    MockMvc mockMvc;


    @MockBean
    OfferService offerService;

    @MockBean
    OfferRepository offerRepository;

    @MockBean
    CryptoService cryptoService;

    @Test
    public void createNewOffer() throws Exception{
        OfferDTO mockDto = new OfferDTO();
        mockDto.setId(1L);
        mockDto.setMessage("Hello i want to");
        mockDto.setAddress("0x11");
        mockDto.setLoanToValue(BigDecimal.valueOf(50));
        mockDto.setLoanAmount(BigDecimal.valueOf(0.005));
        mockDto.setInterestRate(BigDecimal.valueOf(50));
        mockDto.setLiquidationThreshold(BigDecimal.valueOf(0.1));
        mockDto.setDuration(4);
        mockDto.setDurationUnit("MONTH");
        mockDto.setRepaymentCurrencyId(1L);
        mockDto.setCollateralId(17L);

        String inputInJson = this.mapToJson(mockDto);

        String uri = "/pawn/offers";

        Mockito.when(offerService.createNewOffer(Mockito.any(OfferDTO.class))).thenReturn(mockDto);

        RequestBuilder requestBuilder = post(uri)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertEquals(outputInJson,inputInJson);
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        /**
         * "id": 10,
         *   "message": "string",
         *   "address": "string",
         *   "loanToValue": 50,
         *   "loanAmount": 0.005,
         *   "interestRate": 50,
         *   "liquidationThreshold": 0.1,
         *   "duration": 4,
         *   "durationUnit": "MONTH",
         *   "repaymentCurrencyId": 1,
         *   "repaymentCurrencySymbol": "DFY",
         *   "repaymentCurrencyUrl": "https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/DFY.png",
         *   "status": "OPEN",
         *   "createdTime": "2021-10-24T18:34:07.293+00:00",
         *   "collateralId": 17,
         *   "collateralSymbol": "USDC",
         *   "collateralUrl": "https://s3.ap-southeast-1.amazonaws.com/beta-storage-dfy/upload/USDC.png"
         */
    }

    @Test
    public void createNewOfferWithInvalidDuration() throws Exception{
        OfferDTO mockDto = new OfferDTO();
        mockDto.setId(1L);
        mockDto.setMessage("Hello i want to");
        mockDto.setAddress("0x11");
        mockDto.setLoanToValue(BigDecimal.valueOf(50));
        mockDto.setLoanAmount(BigDecimal.valueOf(0.005));
        mockDto.setInterestRate(BigDecimal.valueOf(50));
        mockDto.setLiquidationThreshold(BigDecimal.valueOf(0.1));
        mockDto.setDuration(-4);
        mockDto.setDurationUnit("MONTH");
        mockDto.setRepaymentCurrencyId(1L);
        mockDto.setCollateralId(17L);

        String inputInJson = this.mapToJson(mockDto);

        String uri = "/pawn/offers";

        Mockito.when(offerService.createNewOffer(Mockito.any(OfferDTO.class))).thenReturn(mockDto);

        RequestBuilder requestBuilder = post(uri)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        String durationErrorMessage = "{\"duration\":\"Duration has to be greater than 0\"}";

        assertEquals(outputInJson,durationErrorMessage);
        assertNotEquals(outputInJson, inputInJson);
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }


    //this test failed
    @Test
    public void createNewOfferInvalidDurationUnit() throws Exception {
        OfferDTO mockDto = new OfferDTO();
        mockDto.setId(1L);
        mockDto.setMessage("Hello i want to");
        mockDto.setAddress("0x11");
        mockDto.setLoanToValue(BigDecimal.valueOf(50));
        mockDto.setLoanAmount(BigDecimal.valueOf(0.005));
        mockDto.setInterestRate(BigDecimal.valueOf(50));
        mockDto.setLiquidationThreshold(BigDecimal.valueOf(0.1));
        mockDto.setDuration(4);
        mockDto.setDurationUnit("MON");
        mockDto.setRepaymentCurrencyId(1L);
        mockDto.setCollateralId(17L);

        String inputInJson = this.mapToJson(mockDto);
        String uri = "/pawn/offers";

//        mockMvc.perform(post(uri).accept(MediaType.APPLICATION_JSON).content(inputInJson)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON));


        RequestBuilder requestBuilder = post(uri)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertEquals("Hi", result.getResponse().getContentAsString());

    }

    @Test
    public void getOfferDetails() {
    }

    @Test
    public void getOfferListByCollateral() {
    }

    /**
     * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
     */
    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}