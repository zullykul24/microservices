package com.edso.crypto.services;

import com.edso.crypto.api.v1.model.CoinGeckoDTO;
import com.edso.crypto.api.v1.model.CoinGeckoListDTO;
import com.edso.crypto.api.v1.model.CryptoDTO;
import com.edso.crypto.api.v1.model.CryptoRateHistory;

import java.math.BigDecimal;
import java.util.List;

public interface CoinGeckoService {
    BigDecimal getCurrentPriceVsUsd(String symbol);

    BigDecimal getCurrentPriceVsUsdAtSpecificTime(String symbol, Long timeAt);

    CoinGeckoDTO[] getAllCoins();

    List<CryptoRateHistory> saveAllHistory();

    BigDecimal getToAmount(String fromSymbol, String toSymbol, BigDecimal fromAmount);

    void deleteOldRecords();
}
