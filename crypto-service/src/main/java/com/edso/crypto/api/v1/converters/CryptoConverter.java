package com.edso.crypto.api.v1.converters;

import com.edso.crypto.api.v1.model.CryptoDTO;
import com.edso.crypto.domain.Crypto;
import org.springframework.stereotype.Component;

@Component
public class CryptoConverter {

    public Crypto toCrypto(CryptoDTO source){
        if(source == null) return null;
        Crypto crypto = new Crypto();
        crypto.setId(source.getId());
        crypto.setSymbol(source.getSymbol());
        crypto.setAddress(source.getAddress());
        crypto.setCreatedAt(source.getCreatedAt());
        crypto.setUpdatedAt(source.getUpdatedAt());
        crypto.setDeleted(source.isDeleted());
        crypto.setWhitelistCollateral(source.isWhitelistCollateral());
        crypto.setWhitelistSupply(source.isWhitelistSupply());
        crypto.setName(source.getName());
        crypto.setCoinGeckoId(source.getCoinGeckoId());
        return crypto;
    }

    public CryptoDTO toCryptoDTO(Crypto source){
        if(source == null) return null;
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(source.getId());
        cryptoDTO.setSymbol(source.getSymbol());
        cryptoDTO.setAddress(source.getAddress());
        cryptoDTO.setCreatedAt(source.getCreatedAt());
        cryptoDTO.setUpdatedAt(source.getUpdatedAt());
        cryptoDTO.setDeleted(source.isDeleted());
        cryptoDTO.setWhitelistCollateral(source.isWhitelistCollateral());
        cryptoDTO.setWhitelistSupply(source.isWhitelistSupply());
        cryptoDTO.setName(source.getName());
        cryptoDTO.setCoinGeckoId(source.getCoinGeckoId());
        return cryptoDTO;
    }
}
